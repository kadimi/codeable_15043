<?php get_header(); ?>
	<ul id="sections">
		<li id="s-404" class="section">
			<section>
				<div class="box box-404">
					<div class="column">
						<div id="error-404">
							<h1>404</h1>
							<h2><?php _e('Page Not Found', WAVE_TEXT_DOMAIN); ?></h2>
						</div>
					</div>
				</div>
			</section>
		</li>
	</ul>
	<div class="clearboth"></div>
<?php get_footer(); ?>