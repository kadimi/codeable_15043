"use strict";

jQuery.browser = {msie: false};

if (!console) {
    var console = {};
    console.log = function (object) {
        return object;
    };
}

(function ($) {
    "use strict";

    if(typeof $.fn.safeTrigger == "undefined"){
        $.fn.safeTrigger = function(type){

            var self = this;

            if(!$.trigger_timeouts){
                $.trigger_timeouts = {};
            }

            if(!$.trigger_timeouts[type]){
                $.trigger_timeouts[type] = null;
            }

            if($.trigger_timeouts[type] != null){
                clearTimeout($.trigger_timeouts[type]);
            }

            $.trigger_timeouts[type] = setTimeout(function(){
                $(self).trigger(type);
                setTimeout(function(){
                    //$(self).trigger(type);
                }, 250);
            }, 250);

        };
    }

    var window_width = $(window).innerWidth();
    var window_height = $(window).innerHeight();
    var header_offset = 0;
    var scroll_top = 0;

    if (!$.support.transition) {
        $.fn.transition = $.fn.animate;
    }

    var is_mobile = (Modernizr.touch && window_width < 1300);

    if (window.devicePixelRatio !== undefined) {
        wave_update_dpr_image_src((window.devicePixelRatio > 1) ? 2 : 1);
    }
    else {
        wave_update_dpr_image_src(1);
    }

    function wave_update_dpr_image_src(dpr) {
        $('img[data-src-x' + dpr + ']').each(function () {
            $(this).attr('src', $(this).attr('data-src-x' + dpr));
        });
    }

    // Fix for iOS7
    if(navigator.userAgent.indexOf('OS 7_0')){
        setInterval(function () {
            if (window_width != $(window).innerWidth()) {
                $(window).trigger('resize');
            }
        }, 1000);
    }

    if(!is_mobile && wave_custom_scrollbar){
        $("html").niceScroll({
            zindex: 50000,
            autohidemode: true,
            bouncescroll: true,
            cursorborderradius: '2px',
            cursorwidth: '8px',
            cursorcolor: wave_custom_scrollbar_color,
            cursorborder: '1px solid rgba(255, 255, 255, 0.2)',
            hidecursordelay: 2000
        });
    }

    $('.full-width-section').each(function () {

        if ($(this).next().hasClass("clearboth") && $(this).next().next().length === 0) {
            $(this).addClass("last");
        }

    });

    function zoomDisable() {
        $('head meta[name=viewport]').remove();
        $('head').prepend('<meta name="viewport" content="user-scalable=0" />');
    }

    function zoomEnable() {
        $('head meta[name=viewport]').remove();
        $('head').prepend('<meta name="viewport" content="user-scalable=1" />');
    }

    $("input[type=text], textarea").mouseover(zoomDisable).mousedown(zoomEnable);

    function wave_fix_full_width_section() {
        var windowWidth = $(window).width();
        var columnWidth = $('section.page-section > div.column').width();
        var margin = (windowWidth - columnWidth) / 2;

        $('.full-width-section').css({
            marginLeft: -margin,
            width: windowWidth
        });
    }

    wave_fix_full_width_section();

    $(window).resize(wave_fix_full_width_section);

    $(window).resize();

    $('li.menu-item-cart').hoverIntent({
        over: function () {
            $('#panel-cart').show();
        },
        out: function () {
            $('#panel-cart').hide();
        },
        timeout: 1000

    });


    $('#nav li.nav-button-cart a').append('<div />');


    $("#nav a").click(function () {
        if ($(this).attr("href") == "#") {
            return false;
        }
    });

    $(window).load(function(){

        $('.popup .close-button').click(function () {
            $(this).parents('.popup').trigger('hide');
            return false;
        });

        $('.popup').bind('position', function () {

            var dialog = $(this).find('.popup-content-wrapper');

            if ($(window).width() >= 1100) {
                dialog.css({
                    marginTop: (($(window).height() - dialog.height()) / 2),
                    marginLeft: (($(window).width() - dialog.width()) / 2),
                    width: dialog.attr("data-popup-width")
                });
            }
            else {
                dialog.css({
                    marginTop: 0,
                    marginLeft: 0,
                    width: "100%"
                });
            }

        });

        $('.popup').bind('show', function () {


            $('body').addClass('show_popup');

            var popup = $(this);
            var dialog = popup.find('.popup-content-wrapper');

            if ($(window).width() >= 1100) {
                dialog.width(dialog.attr("data-popup-width"));
            }
            else {
                $(window).scrollTop(0);
            }

            dialog.transition({y: 20, opacity: 0}, 0);
            popup.css({opacity: 0});

            popup.transition({opacity: 1}, 1000);
            dialog.delay(300).transition({y: 0, opacity: 1}, 1000);

        });

        $('.popup').bind('hide', function () {
            $(this).transition({opacity: 0}, 500, function () {
                $(this).hide();
                $('body').removeClass('show_popup');
                $(window).trigger('resize');
            });
        });

    });
    $(window).resize(function () {
        $('.popup').trigger('position');
    });

    $(".call-to-action").click(function () {

        var href = $(this).attr('href');
        var popup = $(href + '.popup');

        if (popup.length > 0) {
            popup.show();
            popup.trigger('show');
            popup.trigger('position');
        }

        return false;

    });

    $(document).bind('gform_page_loaded gform_confirmation_loaded', function () {
        $('.popup').trigger('position');
    });

    window.wave_scrolling = 2;

    $('#back-to-top').click(function () {
        wave_scrolling = true;
        if(wave_disable_scroll_easing){
            $(window).scrollTop(0);
        }
        else{
            $("html, body").animate({scrollTop: 0}, 1000, 'easeOutQuart', function () {
                wave_scrolling = false;
            });
        }
        return false;
    });

    $(window).load(function () {

        $(".page-template-page-onepager-php").each(function () {

            $('#header a#logo-anchor').click(function () {
                window.location.href = $("#header #mainmenu ul li a").first().attr("href");
                return false;
            });

            var lastHash = location.hash.length > 0 ? location.hash : null;
            var menuHash = '';

            setInterval(function () {
                if (menuHash != lastHash) {
                    menuHash = lastHash;
                    $('#nav a[href!=' + menuHash + ']').removeClass("active");
                    $('#nav a[href$=' + menuHash + ']').addClass("active");
                }
            }, 200);

            $("section.page-section").waypoint(function (direction) {

                if (direction != "down" || wave_scrolling) {
                    return;
                }

                window.location.hash = lastHash = "#" + $(this).attr("id").substr(2);

            }, {offset: "50%"});

            $("section.page-section").waypoint(function (direction) {
                if (direction != "up" || wave_scrolling) {
                    return;
                }

                window.location.hash = lastHash = "#" + $(this).attr("id").substr(2);

            }, {offset: function () {
                return -($(this).height() - 100);
            }});

            setInterval(function () {

                if (window.location.hash == "") {
                    window.location.hash = $("#main-content").children("section.page-section:first-child").attr("id").substr(2);
                }

                if (wave_scrolling === 2) {
                    wave_scrolling = false;
                }

                if (lastHash !== window.location.hash) {
                    lastHash = String(window.location.hash);
                    $('#nav a[href!=' + lastHash + ']').removeClass("active");
                    $('#nav a[href=' + lastHash + ']').addClass("active");
                    var section = $("#s-" + lastHash.substr(1));
                    if (section.length > 0) {
                        wave_scrolling = true;
                        var scrollOffset = Math.round(section.index() < 1 ? 0 : section.offset().top - ($('#header').height() - 2));

                        if(wave_disable_scroll_easing){
                            $(window).scrollTop(scrollOffset);
                        }
                        else{
                            $("html, body").animate({scrollTop: scrollOffset}, 1000, 'easeOutQuart', function () {
                                wave_scrolling = false;

                            });
                        }
                    }
                }
            }, 50);

        });
    });


    $('#header a.button-mobilemenu').click(function () {
        if ($('#mobilemenu').css('display') == "block") {
            $('#mobilemenu').css('display', "none");
            $('body').removeClass('show_mobilemenu');
            $(window).trigger('resize');
        }
        else {
            $('#mobilemenu').css('display', "block");
            $('body').addClass('show_mobilemenu');
            if (window.navigator.userAgent.indexOf("Android")) {
                window.location.hash = "#";
            }
        }
        $('html').getNiceScroll().resize();
    });

    $('body').on('touchmove', function (event) {
        if ($('#mobilemenu').css('display') == "block") {
            return;
        }
    });

    $('#mobilemenu').scroll(function (event) {
        event.preventDefault();
        event.stopPropagation();
        return;
    });

    $('#mobilemenu a').click(function () {

        if ($(this).siblings(".sub-menu").length > 0) {

            if ($(this).siblings(".sub-menu").css("display") != "block") {
                $(this).siblings(".sub-menu").css("display", "block");
                return false;
            }
            else {
                $(this).siblings(".sub-menu").css("display", "none");
                return false;
            }

        }
        else {

            $('body').removeClass('show_mobilemenu');
            $(window).trigger('resize');

            if ($(this).attr("href").substr(0, 1) == "#") {
                window.location.hash = $(this).attr("href");
            }

            $('#mobilemenu').css('display', "none");
        }

        $('html').getNiceScroll().resize();

    });


    $('#mobilemenu ul.sub-menu').siblings("a").append('<span class="sub"><i class="icon-chevron-down"></i></span>');


    $(".sf-menu").superfish({
        delay: 100,
        autoArrows: true,
        speed: 'fast',
        animation: {opacity: 'show'}
    });



    $('#mainmenu ul.sf-menu > li a.sf-with-ul').append('<span class="sub"><i class="icon-chevron-down"></i></span>');

    $('ul.sf-menu.menu-icons').mouseenter(function () {

        var buttons = $(this).find(".widget_shopping_cart_content a.button");

        if (!buttons.hasClass("plain")) {
            buttons.addClass("plain");
        }

    });


    if (!is_mobile) {
        $("select:visible").chosen();
    }


    $('.woocommerce div.thumbnails:empty').remove();


    $('.woocommerce div.thumbnails').wrap('<div class="thumbnails-wrapper" />');
    $('.woocommerce a.button.show_review_form').addClass("small");
    $('.woocommerce div.thumbnails-wrapper').append('<a class="button-arrow button-arrow-left" href="#" />');
    $('.woocommerce div.thumbnails-wrapper').append('<a class="button-arrow button-arrow-right" href="#" />');

    $('.woocommerce div.buttons-wrapper a.product_type_variable').addClass("plain").prepend('<i class="icon icon-shopping-cart"></i> ');

    $('.woocommerce ul.page-numbers li a').addClass("button plain");

    $('.woocommerce-tabs').wrap('<div class="tabs-wrapper"/>');
    $('.woocommerce-tabs').addClass('tabs-container');
    $('.woocommerce-tabs .tabs li').attr("class", "").addClass("tab-button");
    $('.woocommerce-tabs .tabs li a').addClass("tab-button-anchor");
    $('.woocommerce-tabs .tabs').attr("class", "").addClass("tabs-buttons");

    $('.woocommerce-tabs').append('<ul class="tabs-contents">').show();

    $('.woocommerce-tabs div.panel').each(function () {
        $('.woocommerce-tabs ul.tabs-contents').append($('<li class="tab-content">').html($(this).html()));
        $(this).remove();
    });

    $(window).resize(function () {
        $('.woocommerce .products .product .catalog-image').each(function () {
            $(this).height($(this).width());
        });
    });

    $('.woocommerce-tabs').removeClass("woocommerce-tabs");


    $("div.tabs-wrapper ul.tabs-buttons").tabs("ul.tabs-contents li.tab-content", {
        tabs: "li.tab-button",
        effect: 'fade',
        current: "active"
    });


    $('.woocommerce div.thumbnails').carouFredSel({
        prev: '.woocommerce div.thumbnails-wrapper a.button-arrow-left',
        next: '.woocommerce div.thumbnails-wrapper a.button-arrow-right',
        circular: true,
        responsive: true,
        width: 424,
        items: {
            width: 116,
            height: 116,
            visible: {
                min: 1,
                max: 4
            }
        },
        swipe: {
            onTouch: true
        },
        scroll: {
            items: 1,
            easing: 'easeInOutCubic',
            duration: 1000,
            pauseOnHover: true
        },
        auto: {
            play: false
        }
    });

    $('input[type="number"]').spinner();

    $('.toggles.single').accordion({
        header: "h3"
    });

    $('.toggles.multi').accordion({
        header: "h3",
        collapsible: true,
        beforeActivate: function (event, ui) {
            if (ui.newHeader[0]) {
                var currHeader = ui.newHeader;
                var currContent = currHeader.next('.ui-accordion-content');
            } else {
                var currHeader = ui.oldHeader;
                var currContent = currHeader.next('.ui-accordion-content');
            }

            var isPanelSelected = currHeader.attr('aria-selected') == 'true';
            currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));
            currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);
            currContent.toggleClass('accordion-content-active', !isPanelSelected)

            if (isPanelSelected) {
                currContent.slideUp();
            } else {
                currContent.slideDown();
            }

            return false;
        }
    });

    $(".woocommerce-message, .woocommerce-error, .woocommerce-info, .alert").click(function (event) {
        var alert = $(this);
        var x2 = alert.outerWidth() - (event.pageX - alert.offset().left);
        var y = event.pageY - alert.offset().top;

        if (x2 <= 34 && x2 >= 20 && y >= 20 && y <= 34) {
            alert.transition({opacity: 0}, 500, function () {
                alert.hide();
            });
        }

        return false;
    });


    $("i.icon").mouseenter(function () {
        var icon = $(this);
        var color = icon.attr("data-color");
        var bgcolor = icon.attr("data-bgcolor");
        var boxcolor = icon.attr("data-boxcolor");

        if (icon.hasClass("mouseover-no")) {
            return false;
        }

        if (icon.hasClass("inherit-color")) {
            icon.transition({backgroundColor: "rgba(0, 0, 0, 0.05)", color: bgcolor}, 500);
        }
        else if (icon.hasClass("inherit-bgcolor")) {
            icon.transition({backgroundColor: color, color: boxcolor}, 500);
        }
    });

    $("i.icon").mouseleave(function () {
        var icon = $(this);
        var color = icon.attr("data-color");
        var bgcolor = icon.attr("data-bgcolor");

        if (icon.hasClass("mouseover-no")) {
            return false;
        }

        icon.transition({backgroundColor: bgcolor, color: color}, 500);
    });

    $(document).bind('wave-prepare-icons', function () {
        $("i.icon.inherit-color, i.icon.inherit-bgcolor").each(function () {

            var icon = $(this);
            var box = icon.parents("div.box");
            var contentBox = icon.parents("div.content-box.boxed");
            var section = $(this).parents("section.page-section");

            var boxcolor = section.css("background-color");
            var sectioncolor = section.css("background-color");

            if (boxcolor == "transparent" || boxcolor == "rgba(0, 0, 0, 0)") {
                boxcolor = sectioncolor;
            }

            if (icon.hasClass("inherit-color")) {

                if (contentBox.length > 0) {
                    icon.css("color", contentBox.css("background-color"));
                }
                else {
                    icon.css("color", boxcolor);
                }
            }

            icon.attr("data-color", icon.css("color"));
            icon.attr("data-bgcolor", icon.css("background-color"));
            icon.attr("data-boxcolor", boxcolor);

        });
    });
    $(document).trigger('wave-prepare-icons');


    if (!is_mobile) {
        $("[data-animation]").each(function () {

            var element = $(this);
            var animation = element.attr("data-animation");

            switch (animation) {
                case "Fade In":
                    $(this).transition({opacity: 0}, 0);
                    break;
                case "Fade In Left":
                    $(this).transition({opacity: 0, x: 20}, 0);
                    break;
                case "Fade In Right":
                    $(this).transition({opacity: 0, x: -20}, 0);
                    break;
                case "Fade In Top":
                    $(this).transition({opacity: 0, y: -20}, 0);
                    break;
                case "Fade In Bottom":
                    $(this).transition({opacity: 0, y: 20}, 0);
                    break;
                case "Zoom":
                    $(this).transition({scale: 0.1, opacity: 0}, 0);
                    break;
            }

        });


        $("[data-animation]").waypoint(function () {

            var element = $(this);
            var animation = element.attr("data-animation");
            var animationTime = element.attr("data-animation-time");
            var animationDelay = element.attr("data-animation-delay");

            var transition = {};

            switch (animation) {
                case "Fade In":
                    transition.opacity = 1;
                    break;
                case "Fade In Left":
                    transition.opacity = 1;
                    transition.x = 0;
                    break;
                case "Fade In Right":
                    transition.opacity = 1;
                    transition.x = 0;
                    break;
                case "Fade In Top":
                    transition.opacity = 1;
                    transition.y = 0;
                    break;
                case "Fade In Bottom":
                    transition.opacity = 1;
                    transition.y = 0;
                    break;
                case "Zoom":
                    transition.opacity = 1;
                    transition.scale = 1;
                    break;
            }

            element.delay(animationDelay).transition(transition, animationTime);


        }, {triggerOnce: true, offset: '90%'});
    }


    $("#sidebar #searchform").each(function () {
        $(this).find("#s").width($(this).width() - 8 - $(this).find("#searchsubmit").outerWidth());
    });


    $('.flexslider').flexslider({
        animation: "fade",
        controlNav: false,
        directionNav: true,
        animationLoop: true,
        slideshow: !$('body').hasClass('archive'),
        start: function () {
            $(this).find('img').show();
        }
    });

    $('.flexslider .flex-direction-nav li a.flex-next').html('<i class="icon-chevron-right"></i>');
    $('.flexslider .flex-direction-nav li a.flex-prev').html('<i class="icon-chevron-left"></i>');

    if ($("body.admin-bar").length > 0) {
        //$("#header").css("margin-top", 28);
    }

    $("#header a.call-to-action").data("height", $("#header a.call-to-action").height());

    var topbar = $('#topbar');
    var has_topbar = $('body').hasClass('topbar');
    var topbar_height = 28;
    var logo = $('#logo');
    var header = $('#header');
    var header_wrapper = $('#header-wrapper');
    var header_nav = $('#nav', header);
    var header_nav_menu = $('ul.sf-menu', header_nav);
    var header_cta = $('a.call-to-action', header);
    var header_mobile_button = $('a.button-mobilemenu');
    var header_position = header.attr('data-header-position');
    var header_large_height = parseInt(header.attr('data-header-large-height'));
    var header_small_height = parseInt(header.attr('data-header-small-height'));
    var logo_large_height = logo.attr('data-logo-large-height');
    var logo_small_height = logo.attr('data-logo-small-height');
    var header_size_difference = header_large_height - header_small_height;
    var header_fixed = false;
    var header_small = false;
    var resize_time = wave_disable_header_easing || window_width < 1100 ? 0 : 500;

    if(!is_mobile){
        $('#header').removeClass('small');
    }

    switch(header_position){
        case 'below':
        case 'appear':
        case 'bottom':
            header_wrapper.height(header_large_height);
            break;
        case 'top':
            header_wrapper.height(header_small_height);
            break;
    }


    if(header_position == 'top'){
        //$('#slider').css('margin-top', -header_size_difference);
    }

    var back_to_top_active = false;
    var scroll_counter = 0;
    var submenu_offset = 0;

    setInterval(function(){
        if(scroll_counter < 10){
            submenu_offset = header.position().top + header.height();
            submenu.css('top', submenu_offset);
        }
    }, 50);

    $(window).scroll(function(){

        scroll_top = $(window).scrollTop();
        header_offset = $('#header-wrapper').offset().top;

        scroll_counter = 0;

        switch(header_position){
            case 'below':
            case 'appear':
                header_offset = window_height;
                break;
            case 'top':
                header_offset = 2 + (has_topbar ? topbar_height : 0);
                break;
            case 'bottom':
                break;
        }


        if(scroll_top > header_offset){
            if(!header_fixed){
                header_fixed = true;
                header.trigger('header-resize');
            }
        }
        else if(header_fixed){
            header_fixed = false;
            header.trigger('header-resize');
        }

        if(scroll_top < 100 && back_to_top_active){
            back_to_top_active = false;
            $('#back-to-top').fadeOut();
        }
        else if(scroll_top >= 100 && !back_to_top_active){
            back_to_top_active = true;
            $('#back-to-top').fadeIn();
        }

    });

    $('#header').bind('header-resize', function(){

        var config = {};
        config.time = resize_time;
        config.cta_large = 42;
        config.cta_small = 36;
        config.button_mobile_size = 26;
        config.cta_small_padding = '7px 12px';
        config.cta_large_padding = '11px 16px';
        config.header_top = 0;

        if(!header_fixed && !is_mobile && window_width > 1100){
            config.header_height = header_large_height;
            config.logo_height = logo_large_height;
            config.logo_margin_top = (header_large_height - logo_large_height) / 2;
            config.cta_margin_top = (header_large_height - config.cta_large) / 2;
            config.cta_padding = config.cta_large_padding;
            if(header_position == 'appear'){
                header.css('position', 'fixed');
                config.header_top = -(parseInt(header_large_height) + 10);
            }
            else{
                header.css('position', 'relative');
            }
            config.mobile_button_margin = (header_large_height - config.button_mobile_size) / 2;
        }
        else{
            config.header_height = header_small_height;
            config.logo_height = logo_small_height;
            config.logo_margin_top = (header_small_height - logo_small_height) / 2;
            config.cta_margin_top = (header_small_height - config.cta_small) / 2;
            config.cta_padding = config.cta_small_padding;
            header.css('position', 'fixed');
            if(header_position == 'appear'){
                config.header_top = 0;
            }
            config.mobile_button_margin = (header_small_height - config.button_mobile_size) / 2;

            if ($("body.admin-bar").length > 0) {
                config.header_top += 28;
            }
        }

        header.stop(true, true).transition({top: config.header_top, height: config.header_height}, config.time);
        header_nav.stop(true, true).transition({height: config.header_height}, config.time);
        header_nav_menu.stop(true, true).transition({lineHeight: config.header_height + 'px'}, config.time);
        logo.stop(true, true).transition({height: config.logo_height, marginTop: config.logo_margin_top}, config.time);
        header_cta.stop(true, true).transition({marginTop: config.cta_margin_top, padding: config.cta_padding}, config.time);
        header_mobile_button.stop(true, true).transition({marginTop: config.mobile_button_margin, marginLeft: config.mobile_button_margin});

        if(window_width <= 690){
            $('#footer-mobile-menu').transition({height: config.header_height, lineHeight: config.header_height + 'px'}, config.time);
        }

    });

    $('#header').trigger('header-resize');

    $(window).resize(function () {
        $(window).scroll();

        if (!$('.right.right-mobile').is(":visible")) {
            $('#mobilemenu').hide();
        }

    });

    var submenu = $('#mainmenu ul.sub-menu, #mainmenu #panel-cart');
    var submenu_links = $('#mainmenu li.menu-item > ul.sub-menu, #mainmenu li.menu-item > #panel-cart').siblings('a');

    submenu_links.mouseenter(function(){
        $(this).siblings('ul.sub-menu, #panel-cart').css('left', $(this).offset().left);
    });

    $("a.lang_sel_sel .iclflag").replaceWith('<i class="icon icon-globe"></i>');


    $("a.lang_sel_sel").click(function () {
        return false;
    });

    $(window).resize(function () {

        var sliderChild = $('#slider > div');

        if (sliderChild.height() > 0) {
            $('#slider').height($('#slider > div').height());
        }

    });

    $(document).ready(function () {
        $(window).resize();
    });

    $(window).load(function () {
        $(window).resize();
    });

    $('[data-parallax-background-ratio]').each(function () {

        var element = $(this);
        var source = element.attr('data-background-image');

        element.data('parallax-scale', element.attr('data-parallax-scale') == 'true');

        var image = new Image();
        image.onload = function () {
            element.data('background-image', this);
            element.attr('data-parallax-enabled', 1);
            element.css('background-image', 'url(' + this.src + ')');
        };
        image.src = source;

    });

    $(window).on('parallax-update', function () {

        var win = $(window);
        var windowHeight = win.innerHeight();
        var windowWidth = win.innerWidth();
        var windowTop = $(document).scrollTop() + windowHeight;
        var parallax_ratio = 1;

        $('[data-parallax-enabled]').each(function () {

            var element = $(this);
            var image = element.data('background-image');
            var height = element.height();
            var top = element.offset().top;
            var offset_start = (top < windowHeight ? windowHeight - top : -0.01);
            var offset = (windowTop - top > 0 ? windowTop - top : 0) - offset_start;
            var ratio = offset / ((windowHeight + height) - offset_start);
            var imageWidth = image.width;

            if (ratio > 1 || ratio < 0) {
                return;
            }

            var imageHeight = image.height * (windowWidth / image.width);

            if (imageHeight < height) {
                imageHeight = height;
                imageWidth = image.width * (imageHeight / image.height);
            }

            element.css('background-size', imageWidth + 'px ' + imageHeight + 'px');
            element.css('background-position', '50% -' + (((imageHeight - height) * ratio) * parallax_ratio) + "px");

        });

    });

    if(!is_mobile){
        $(window).scroll(function () {
            $(window).trigger('parallax-update');
        });
    }



    var time = function(){
        return Math.round((new Date()).getTime()/1000);
    };

    var last_trigger = time;

    $(window).on('wave-screen-small', function(){

        if(time() - last_trigger < 2){
            return;
        }

        last_trigger = time();

        $('.raw-video-background.ready').each(function(){
            $(this).removeClass('ready');
            $(this).addClass('not-ready');
            $(this).html('');
        });
    });

    $(window).on('wave-screen-large', function(){

        if(time() - last_trigger < 2){
            return;
        }

        last_trigger = time();


        $(window).trigger('wave-create-videos');

    });

    $(window).bind('wave-create-videos', function(){

        $('.raw-video-background.not-ready').each(function(){

            $(this).removeClass('not-ready');
            $(this).addClass('ready');

            var videobg = $(this);


            if($(this).find('video').length == 0){

                $(this).videoBG({
                    mp4: $(this).attr('data-video-mp4'),
                    webm: $(this).attr('data-video-webm'),
                    poster: $(this).attr('data-video-poster'),
                    scale: true,
                    autoplay: false,
                    zIndex: 0
                });

                var video = $(this).find('video');
                var element = video.get(0);

                element.addEventListener('play', function(){
                    if($(this).data('raw-video-canplay') !== true){
                        this.pause();
                    }
                }, false);

                element.addEventListener('canplay', function(){
                    if($(this).data('raw-video-canplay') == true){
                        this.play();
                    }
                    else{
                        this.pause();
                    }
                }, false);

                video.bind('raw-video-play', function(){
                    $(this).data('raw-video-canplay', true);
                    if(!this.playing){
                        this.play();
                    }
                });

                video.bind('raw-video-pause', function(){
                    $(this).data('raw-video-canplay', false);
                    this.currentTime = 0;
                    this.pause();
                });

                if($(video).parents('.raw-video-background').length == 0){
                    element.pause();
                }
                else{
                    video.trigger('raw-video-play');
                }

            }

            $(this).attr('data-raw-video-status', '1');

        });

        $('.full-width-background .raw-video-background video').safeTrigger('raw-video-play');

    });


    $(window).resize(function(){
        if(window_width != $(window).innerWidth() || window_height != $(window).innerHeight()){
            window_width = $(window).innerWidth();
            window_height = $(window).innerHeight();
            if(window_width < 1100){
                if(!$('#header').hasClass('mobile')){
                    $('#header').addClass('mobile');
                }
            }
            else{
                $('#header').removeClass('mobile');
            }

            $(window).trigger('raw-window-resize');
            setTimeout(function(){
                $(window).trigger('raw-window-resize');
            }, 200);
        }
    });

    $(window).load(function(){

        var last_resize = time();
        var resize_timeout = null;

        $(window).on('raw-window-resize', function(){

            if(header_position != 'top' && window_width <= 1100){
                ///$('#slider').css('margin-top', header_small_height);
                header.css({position: 'fixed', top: 0});
            }
            else{
                //$('#slider').css('margin-top', 0);
            }

            if(window_width <= 1100){
                topbar.hide();
            }
            else{
                topbar.show();
            }

            $('#header').trigger('header-resize');

            setTimeout(function(){

                var width = $(window).innerWidth();

                $('.videoBG_wrapper, .videoBG').css('width', '100%');

                $('.videoBG').each(function(){
                    $(this).find('video').css({
                        zIndex: 0,
                        position: 'absolute',
                        left: '50%',
                        marginLeft: '-50%'
                    });
                });

                if(width < 1100){
                    $(window).trigger('wave-screen-small');
                }
                else{
                    $(window).trigger('wave-screen-large');
                }

                $('.full-width-background .raw-video-background video').safeTrigger('raw-video-play');

            }, 100);

        });

        $(window).trigger('raw-window-resize');

    });


})(jQuery);





