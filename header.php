<?php do_action('wave_before_html'); ?><!doctype html>
<!--[if lt IE 7 ]>
<html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>
<html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head id="html-head">
	<meta charset="<?php bloginfo('charset'); ?>">
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->
	<?php if (is_search()) : ?>
		<meta name="robots" content="noindex, nofollow"/>
	<?php endif; ?>
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>"/>
	<meta name="viewport" content="initial-scale=1"/>
	<meta property="og:title" content="Cloud Summit 2014" />
	<meta property="og:description" content="This is the channel�s largest cloud computing event and also your powerhouse, concentrated chance to refine your cloud expertise with access to unmatched cloud resources designed to help you grow your business�further and faster." />
	<meta property="og:url" content="http://im-cloudsummit.com" />
	<meta property="og:image" content="http://im-cloudsummit.com/wp-content/uploads/2014/01/reneeNew.jpg" />
	<?php if (wave_info('ios_icon_57')) : ?>
		<link rel="apple-touch-icon" href="<?php echo wave_info("ios_icon_57"); ?>"/>
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo wave_info("ios_icon_57"); ?>"/>
	<?php endif; ?>
	<?php if (wave_info('ios_icon_72')) : ?>
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo wave_info("ios_icon_72"); ?>"/>
	<?php endif; ?>
	<?php if (wave_info('ios_icon_114')) : ?>
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo wave_info("ios_icon_114"); ?>"/>
	<?php endif; ?>
	<?php if (wave_info('ios_icon_144')) : ?>
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo wave_info("ios_icon_144"); ?>"/>
	<?php endif; ?>
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>"/>
	<script type="text/javascript">var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';</script>
	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
	<?php do_action('wave_before_html_head_end'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action('wave_after_html_body'); ?>
<div id="wrapper">
	<?php do_action('wave_add_topbar'); ?>
	<?php do_action('wave_header_position_top'); ?>
	<div id="footer-mobile-menu">
		<?php do_action('wave_footer_mobile_menu'); ?>
	</div>
	<?php wave_page_slider(); ?>
	<?php do_action('wave_header_position_bottom'); ?>
	<?php if (wave_option('header_topbar_enabled')): ?>
		<div id="main-content-wrapper" class="has-topbar">
	<?php else: ?>
		<div id="main-content-wrapper">
	<?php endif; ?>
	<div id="main-content">
