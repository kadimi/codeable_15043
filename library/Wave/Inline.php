<?php

class Wave_Inline {

	private $ueid;
	private $selector;
	private $suffix;
	private $pseudo_classes;

	public function __construct($prefix = '#', $pseudo_classes = array(''), $suffix = '') {
		$this->ueid     = wave_ueid();
		$this->suffix = empty($suffix) ? '' : ' ' . $suffix;
		$this->selector = $prefix . $this->ueid;
		$this->pseudo_classes = $pseudo_classes;
	}

	public function css($key, $value = null) {

		if(is_array($key) && $value == null){
			foreach($key as $property => $value){
				$this->css($property, $value);
			}
			return;
		}

		$value = addslashes($value);

		$selectors = array();

		foreach($this->pseudo_classes as $pseudo_class){
			if(empty($pseudo_class)){
				$selectors[] = $this->selector . $this->suffix;
			}
			else{
				$selectors[] = $this->selector . ':' . $pseudo_class . sprintf($this->suffix, $pseudo_class);
			}
		}

		Wave_Dynamic::css(join(', ', $selectors), $key, $value);
	}

	public function ueid() {
		echo $this->ueid;
	}

	public function get_ueid() {
		return $this->ueid;
	}

	public function get_style_attribute() {

	}

}