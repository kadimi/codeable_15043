<?php do_action('wave_before_header'); ?>
<script>
var wave_header_position = '<?php echo wave_option('header_position') ?>';
</script>
<div id="header-wrapper">
<header id="header"
        class="header-position-<?php echo wave_option('header_position') ?> small"
        data-header-position="<?php echo wave_option('header_position') ?>"
        data-header-large-height="<?php echo wave_option('style_header_large_height') ?>"
        data-header-small-height="<?php echo wave_option('style_header_small_height') ?>">
	<?php do_action('wave_header_start'); ?>
	<div id="header-content-wrapper">
		<div id="header-content">
			<nav id="nav" role="navigation">
				<?php wave_primary_menu(); ?>
				<div>
					<ul class="sf-menu menu-icons">
						<?php do_action("wave_header_menu_icons"); ?>
					</ul>
				</div>
			</nav>
			<div class="left">
				<a id="logo-anchor" href="<?php echo esc_url(home_url('/')); ?>"
				   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
					<?php if (wave_option('header_logo_image')): ?>
						<img id="logo"
						     src="<?php echo wave_option('header_logo_image') ?>"
						     data-src-x1="<?php echo wave_option('header_logo_image') ?>"
						     data-src-x2="<?php echo wave_option('header_logo_image_retina') ?>"
						     data-logo-large-height="<?php echo wave_option('style_logo_large_height') ?>"
						     data-logo-small-height="<?php echo wave_option('style_logo_small_height') ?>"
						     alt=""/>
					<?php else: ?>
						<span id="logo"
						      data-logo-large-height="<?php echo wave_option('style_logo_large_height') ?>"
						      data-logo-small-height="<?php echo wave_option('style_logo_small_height') ?>"><?php echo esc_attr(get_bloginfo('name', 'display')); ?></span>
					<?php endif; ?>
				</a>
			</div>
			<div class="right right-default">
				<?php do_action('wave_header_right_default'); ?>
			</div>
			<div class="right right-mobile">
				<?php do_action('wave_header_right_mobile'); ?>
				<a class="button-mobilemenu" href="javascript:void(0);"><i class="icon icon-reorder"></i></a>
			</div>
		</div>
	</div>
	<?php do_action('wave_header_end'); ?>
</header>
</div>
<?php do_action('wave_after_header'); ?>