<?php

$margin_top = get_post_meta(get_the_ID(), 'margin_top', true);
$margin_bottom = get_post_meta(get_the_ID(), 'margin_bottom', true);
$parallax_enabled = get_post_meta(get_the_ID(), "enable_parallax", true) === "1";
$attributes = '';

$column_inline = new Wave_Inline();

if($margin_top != '80' || $margin_bottom != '80'){
	$column_inline->css('padding-top', $margin_top . 'px');
	$column_inline->css('padding-bottom', $margin_bottom . 'px');
}

$inline = new Wave_Inline('#main-content section.');

if (get_post_meta(get_the_ID(), "background_color", true)) {
	$inline->css('background-color', get_post_meta(get_the_ID(), "background_color", true));
}

if (get_post_meta(get_the_ID(), "background_image", true)) {
	if ($parallax_enabled) {
		$attributes .= ' data-parallax-background-ratio="0.7"';
		$attributes .= ' data-background-image="' . wave_get_post_meta_image('background_image') . '"';
		$attributes .= ' data-parallax-scale="true"';
	} else {
		$inline->css('background-image', 'url(' . wave_get_post_meta_image('background_image') . ')');
	}
}



?>
	<section class="page-section <?php echo $inline->ueid(); ?>"<?php echo $attributes; ?>
	         id="s-<?php wave_the_slug(); ?>">
		<div class="column" id="<?php echo $column_inline->get_ueid() ?>">
			<?php wave_page_title(); ?>
			<?php the_content(); ?>
			<div class="clearboth"></div>
		</div>
	</section>
<?php if (get_post_meta(get_the_ID(), "enable_separator", true) === "1"): ?>
	<div
		class="full-width-separator <?php echo(get_post_meta(get_the_ID(), "enable_separator_parallax", true) === "1" ? " parallax" : ""); ?>"
		data-parallax-background-ratio="0.7"
		data-background-image="<?php echo wave_get_post_meta_image("separator_background"); ?>">
		<div
			class="content<?php echo(get_post_meta(get_the_ID(), "enable_separator_pattern", true) === "1" ? " pattern" : ""); ?>">
			<div class="column">
				<?php echo do_shortcode(wave_content_filter(get_post_meta(get_the_ID(), "separator_content", true))); ?>
			</div>
		</div>
	</div>
<?php endif; ?>