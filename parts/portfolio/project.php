<div class="box box-portfolio">
	<div class="column">
		<?php get_template_part("parts/portfolio/content"); ?>
		<div class="clearboth"></div>
	</div>
</div>
<div class="box box-comments">
	<div class="column">
		<div class="three_fourth">
			<?php comments_template(); ?>
		</div>
		<div class="clearboth"></div>
	</div>
</div>